
var js = document.getElementById("cus-div").dataset.myVar;
$.ajax({
  url: '/get_data',
  method: 'POST',
  data: { symbol: js },


  success: function(data) {
    console.log("data:", data);

    var trace = {
      type: "line",
      name: data.symbol,
      x: data.dates,
      y: data.prices,
      line: { color: "green" }
    };
    
    var layout = {
      title: data.symbol,
      xaxis: { title: "Date" },
      yaxis: { title: "Closing Price" }
    };
    
    var data = [trace];
    Plotly.newPlot(document.getElementById('myDiv'), data, layout);
  }
});

