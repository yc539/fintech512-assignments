from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify

import json
import requests
import config

API_KEY = config.api_key
app = Flask(__name__)


@app.route('/')
def main():
    return render_template('main.html')
    
def get_prices(symbol, API_KEY):
    prices_url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&outputsize=full&apikey={API_KEY}'
    r = requests.get(prices_url)
    data = r.json()

    # get dates,prices lists
    dates = []
    prices = []
    for date, values in list(data['Time Series (Daily)'].items())[:250]:
        dates.append(date)
        prices.append(float(values['4. close']))
    return dates, prices

@app.route('/get_stock', methods=['GET', 'POST'])
def get_stock():
    if request.method == 'POST':
        symbol = request.form['symbol']
       # create a data dictionary
        data = {}
        overview = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={API_KEY}'
        global_quote = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={API_KEY}'
        r1 = requests.get(overview)
        r2 = requests.get(global_quote)
        data['overview'] = json.loads(r1.text)
        data['global_quote'] = json.loads(r2.text)
        news = requests.get(f'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&sort=LATEST&apikey={API_KEY}').json()
        data['news'] = news['feed'][:5]
        return render_template('stock.html', stock = data, symbol = symbol)
    else:
        return redirect(url_for('main'))


@app.route('/get_data', methods=['GET', 'POST'])
def get_data():
    if request.method == "POST":
        symbol = request.form['symbol']
        dates, prices = get_prices(symbol, API_KEY)
        data = {"dates":dates, "prices":prices, "symbol": symbol}
        return jsonify(data)
    else:
        return jsonify(error="Invalid request method")

