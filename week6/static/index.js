$(function() {
  $('#stock-table').DataTable({
    responsive: true,
    searching: false,
    paging: false,
    info: false
  });
});
