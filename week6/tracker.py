from flask import Flask, render_template, request, redirect
import sqlite3
import requests
import plotly.graph_objs as go
import plotly.express as px
from datetime import datetime
import pytz
from tzlocal import get_localzone
import json


app = Flask(__name__)

@app.template_filter('datetimeformat')
def datetimeformat(value, format='%Y-%m-%d %H:%M:%S %Z'):
    # Parse the input string as a UTC datetime
    utc_time = datetime.strptime(value, '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.UTC)
    
    # Convert the UTC datetime to the local time zone
    local_tz = pytz.timezone(get_localzone().zone)  # replace with your desired time zone
    local_time = utc_time.astimezone(local_tz)
    
    # Format the local time as a string
    return local_time.strftime(format)


def get_stock_info(symbol):
    data = {}
    global_quote = f"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey=config.api_key"
    response = requests.get(global_quote)
    data['global_quote'] = json.loads(response.text)
    if response.status_code == 200:
        return data
    else:
        return None

def add_stock(symbol, tracking_price, shares):
    stock_info = get_stock_info(symbol)
    if stock_info:
        current_price = float(stock_info['global_quote']['Global Quote']["05. price"])
        percent_change = round((current_price - tracking_price) / tracking_price * 100,2)
        conn = sqlite3.connect("stock_tracker.db")
        c = conn.cursor()
        c.execute("INSERT INTO stock_tracker (symbol, tracking_price, shares, current_price, percent_change) VALUES (?,?,?,?,?)", (symbol, tracking_price, shares, current_price, percent_change))
        conn.commit()
        conn.close()
        return True
    else:
        return False

def delete_stock(id):
    conn = sqlite3.connect("stock_tracker.db")
    c = conn.cursor()
    c.execute("DELETE FROM stock_tracker WHERE id=?", (id,))
    conn.commit()
    conn.close()


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        if "symbol" in request.form:
            symbol = request.form["symbol"]
            tracking_price = float(request.form["tracking_price"])
            shares = int(request.form["shares"])
            try:
                if add_stock(symbol, tracking_price, shares):
                    return redirect("/")
            except Exception as e:
                return "Error: Failed to add stock: {}".format(str(e))
        elif "id" in request.form:
            id = request.form["id"]
            delete_stock(id)
            return redirect("/")

        tracking_price = float(request.form["tracking_price"])
        shares = int(request.form["shares"])
        try:
            if add_stock(symbol, tracking_price, shares):
                return redirect("/")
        except Exception as e:
            return "Error: Failed to add stock: {}".format(str(e))
    else:
        conn = sqlite3.connect("stock_tracker.db")
        c = conn.cursor()
        c.execute("SELECT * FROM stock_tracker")
        stocks = c.fetchall()
        conn.close()
        

        scatter_fig = price_chart()
        pie_fig = pie_chart()
        
        scatter_html = scatter_fig.to_html(full_html=False)
        pie_html = pie_fig.to_html(full_html=False) 
        return render_template("index.html", stocks=stocks, scatter_html=scatter_html, pie_html=pie_html)



def price_chart():
    conn = sqlite3.connect("stock_tracker.db")
    c = conn.cursor()
    c.execute('SELECT symbol, date_added, tracking_price FROM stock_tracker')
    rows = c.fetchall()
    conn.close()

    fig = go.Figure()
    for row in rows:
        symbol = row[0]
        date_added = row[1]
        tracking_price = row[2]
        x = datetimeformat(date_added)
        fig.add_trace(go.Scatter(x=[x], y=[tracking_price], mode='markers', name=symbol))
    
    # Set the chart title and axis labels
    fig.update_layout(
        width=600, 
        height=400, 
        margin=dict(l=40, r=40, t=60, b=40), 
        title = 'Stock Price', 
        xaxis_title='Date', 
        yaxis_title='Tacking Price')
    return fig

def pie_chart():
    conn = sqlite3.connect("stock_tracker.db")
    c = conn.cursor()
    c.execute('SELECT SUM(shares), symbol FROM stock_tracker GROUP BY symbol')
    rows = c.fetchall()
    conn.close()

    fig = px.pie(
        values=[row[0] for row in rows],
        names=[row[1] for row in rows]
    )

    fig.update_layout(
        width=600, 
        height=400, 
        margin=dict(l=40, r=40, t=60, b=40), 
        title="Distribution of Stocks"
    )

    return fig

